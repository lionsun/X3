layui.define(function(exports) {
	jQuery = layui.jquery;
	$ = layui.jquery;
	var fn = {};
	load_js('layui/plugins/flot/jquery.flot.js', function() {
		load_js('layui/plugins/flot/jquery.flot.tooltip.min.js', function() {
			load_js('layui/plugins/flot/jquery.flot.resize.js', function() {
				load_js('layui/plugins/flot/jquery.flot.pie.js', function() {
					load_js('layui/plugins/flot/jquery.flot.time.js', function() {
						fn.ready();
					});
				});
			});
		});
	});
	exports('float', fn);
});

