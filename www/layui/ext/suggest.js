layui.define(function(exports) {//提示：组件也可以依赖其它组件，如：layui.define('layer', callback);
	$ = layui.jquery;
	$.fn.suggest = function(options) {
		var settings = $.extend({
			'min_chars' : 2,
			'display_qty' : 6,
		}, options);

		var url = settings.url;
		var min_chars = settings.min_chars;
		var display_qty = settings.display_qty;
		var callback = settings.callback;
		html = '<div class="search dropdown "><ul class="dropdown-menu"></ul></div>';
		this.after(html);

		var $search = $(this).next();
		/* 查找联系人input 功能*/
		$search.on("click", "li", function() {
			if ($search.find("ul").html() != "") {
				$this = $(this);
				key_data = $this.attr('key');
				val_data = $this.attr('val');

				var ret = {};
				ret.status = 1;
				ret.info = '';
				ret.data = {
					'key' : key_data,
					'val' : val_data
				};
				$search.html("");
				$search.hide();
				callback(ret);
			}
		});

		var $add_on = $search.next();

		$add_on.on("click", function() {
			$.getJSON(url, {
				key : $(this).val()
			}, function(ret) {
				if (ret.status == 1) {
					if (ret.data.length > 0) {
						$search.html("");
						var qty = 0;
						$.each(ret.data, function(i) {
							qty++;
							if (display_qty > qty) {
								$search.append('<li key="' + ret.data[i].id + '" val="' + ret.data[i].name + '" ><a >' + ret.data[i].name + '</a></li>');
							}
						});
						$search.children("li:first").addClass("active");
						$search.show();
					}
				} else {
					$search.html("");
					$search.hide();
				}
			});
		});
		/* 查找联系人input 功能*/
		this.keyup(function(e) {
			var $this = $(this);
			var search_key = $this.val();
			var $search = $this.next();
			if (search_key.length >= settings.min_chars) {
				console.log(e.keyCode);
				switch(e.keyCode) {
				case 40:
					var $curr = $search.find("li.active").next();
					if ($curr.html() != null) {
						$search.find("li").removeClass("active");
						$curr.addClass("active");
					}
					return false;
					break;
				case 38:
					var $curr = $search.find("li.active").prev();
					if ($curr.html() != null) {
						$search.find("li").removeClass("active");
						$curr.addClass("active");
					}
					return false;
					break;
				case 13:
					if ($search.find("ul").html() != "") {
						key_data = $search.find("li.active").attr('key');
						val_data = $search.find("li.active").attr('val');

						var ret = {};
						ret.status = 1;
						ret.info = '';
						ret.data = {
							'key' : key_data,
							'val' : val_data
						};
						$search.html("");
						$search.hide();
						callback(ret);

					}
					return false;
					break;
				default:
					console.log($(this).val());
					if ($(this).val().length >= min_chars) {
						$.getJSON(url, {
							key : $(this).val()
						}, function(ret) {
							if (ret.status == 1) {
								if (ret.data.length > 0) {
									$search.html("");
									var qty = 0;
									$.each(ret.data, function(i) {
										qty++;
										if (display_qty > qty) {
											$search.append('<li key="' + ret.data[i].id + '" val="' + ret.data[i].name + '" ><a >' + ret.data[i].name + '</a></li>');
										}
									});
									$search.children("li:first").addClass("active");
									$search.show();
								}
							} else {
								$search.html("");
								$search.hide();
							}
						});
					}
				}
			} else {
				$search.hide();
			}
		});
	};
	exports('suggest', {});
});

