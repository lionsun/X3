layui.define(function(exports) {
	var obj = {
		get_badge_count : function() {
			$.post(badge_count_url, function(ret) {
				//console.log(ret);
				for (s in ret) {
					if (ret[s] > 0) {
						$('a[node="' + s + '"] .count').text(ret[s]);
					}
				}
			}, 'json');
		}
	};
	obj.get_badge_count();
	exports('badge', obj);
});

