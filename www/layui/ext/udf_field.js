layui.define(function(exports) {//提示：组件也可以依赖其它组件，如：layui.define('layer', callback);
	$ = layui.jquery;
	var udf_field = {
		init : function() {
			udf_field.init_link_select();
			udf_field.init_other();
		},
		init_link_select : function() {
			$(".link_select").each(function() {
				$data = $(this).attr('data');
				$pid = $(this).attr('pid');
				$val = $(this).attr('val');

				var json = $.parseJSON($data);
				var $main = $(this).find(".main");
				var $sub = $(this).find(".sub");
				var fill_option = function($obj, $pid) {
					$($obj).html("<option>请选择</option>");
					for (s in json) {
						if (json[s].pid == $pid) {
							$option = $("<option></option>");
							$option.text(json[s].name);
							$option.val(json[s].id);
							$($obj).append($option);
						}
					}
					if ($val !== undefined) {
						$field_id = $obj.attr("id").replace("udf_field_", "").replace("sub", "");
						if ($val.indexOf(',')) {
							$udf_data = $val.split(',');
							$("#udf_field_" + $field_id).val($udf_data[0]);
							$("#udf_field_" + $field_id + 'sub').val($udf_data[1]);
						} else {
							$obj.val($udf_data);
						}
					}
					$obj.change();
				};

				$main.change(function() {
					$current = $main.val();
					fill_option($sub, $current);
				});

				//转换为json对象
				fill_option($main, $pid);
			});
		},
		init_other : function() {
			$(".udf_other .other").click(function() {
				if (this.checked) {
					$(this).parents('.udf_other').find('.other_text').removeClass('hidden');
				} else {
					$(this).parents('.udf_other').find('.other_text').addClass('hidden');
				}
			});
			$(".udf_other .normal").click(function() {
				$parent = $(this).parents('.udf_other');
				$is_checked = $parent.find('.other').prop('checked');
				if ($is_checked) {
					$(this).parents('.udf_other').find('.other_text').removeClass('hidden');
				} else {
					$(this).parents('.udf_other').find('.other_text').addClass('hidden');
				}
			});
		}
	};

	udf_field.init();

	$('.btn-udf-popup').on('click', function() {
		id = this.id;
		var $this = $(this);
		data = $this.attr('data');
		url = $this.attr('url');

		var vars = {};
		vars.id = id;
		vars.data = data;
		winopen(fix_url(url, vars), 560, 470);
	});
	exports('udf_field', udf_field);
});
