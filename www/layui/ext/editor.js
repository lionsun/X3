layui.define(function(exports) {
	jQuery = layui.jquery;
	$ = layui.jquery;
	load_js('layui/ext/plugins/tinymce/tinymce.min.js', function() {
		if (is_mobile()) {
			$editor = {
				menu : {},
				language : "zh_CN",
				selector : ".editor",				
				plugins : ["autosave imageupload autolink lists link charmap print preview anchor", "searchreplace visualblocks fullscreen", "insertdatetime media table paste"],
				toolbar : "undo redo | styleselect | fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link imageupload",
				contextmenu : false
			};
			$simple = {
				menu : {},
				language : "zh_CN",
				selector : ".simple",
				plugins : ["autosave imageupload advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks fullscreen", "insertdatetime media table paste"],
				toolbar : false,
				content_css : '/Public/layui/css/global.css',
			};

		} else {
			$editor = {
				language : "zh_CN",
				selector : ".editor",
				plugins : ["autosave imageupload advlist autolink lists link image charmap print preview anchor textcolor", "searchreplace visualblocks fullscreen", "insertdatetime media table contextmenu paste"],
				toolbar : "undo redo | styleselect | fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link imageupload",
				contextmenu : "inserttable | cell row column deletetable"
			};
			$simple = {
				menu : {},
				language : "zh_CN",
				selector : ".simple",
				plugins : ["autosave imageupload advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
				toolbar : "styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link imageupload",
				contextmenu : "inserttable | cell row column deletetable"
			};
		}
		tinymce.init($editor);
		tinymce.init($simple);
	});
	exports('editor', {});
});
