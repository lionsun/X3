<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class project_controller extends base_controller {
	protected $app_type = 'common';
	protected $auth_map = array('admin' => 'test,del,move_to', 'read' => 'index,folder_manage,select_member','write'=>'add_task,edit_task,save_task');

	function _search_filter(&$map) {
		$keyword = request('keyword');
		if (!empty($keyword) && empty($map['name'])) {
			$map[] = array('name', 'like', "%" . $keyword . "%");
		}
	}

	public function index() {
		$auth = $this -> auth;
		$this -> assign('auth', $auth);

		$project_id1 = array();
		$project_id2 = array();
		$where_project[] = array('user_id', 'eq', get_user_id());

		$project_id1 = model('project_member') -> where($where_project) -> get_field('project_id', true);

		$project_id2 = model('project') -> where($where_project) -> get_field('id', true);

		if (empty($project_id1)) {
			$project_id1 = array();
		}
		if (empty($project_id2)) {
			$project_id2 = array();
		}

		$project_id = array_unique(array_merge($project_id1, $project_id2));

		$where = $this -> _search();
		if (!empty($project_id)) {
			$where[] = array('id', 'in', $project_id);
		} else {
			//$where['_string'] = '1=2';
		}

		$where[] = array('is_del', 'eq', 0);

		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($where);
		}	
		$model = model('project');
		$this -> _list($model, $where, 'id desc');
		$this -> display();
	}

	function add() {

		$model_flow_field = model("udf_field");
		$field_list = $model_flow_field -> get_field_list(0);
		$this -> assign("field_list", $field_list);
		$this -> display();
	}

	//项目详细
	public function read($id) {
		$model_project_task = model('project_task');

		//全部任务
		$where_task_all[] = array('project_id', 'eq', $id);
		$where_task_all[] = array('is_del', 'eq', 0);
		$task_list_all = $model_project_task -> where($where_task_all) -> order('end_date') -> get_list();
		$this -> assign('task_list_all', $task_list_all);

		//待办
		$where_task_todo[] = array('project_id', 'eq', $id);
		$where_task_todo[] = array('is_finish', 'eq', 0);
		$where_task_todo[] = array('is_del', 'eq', 0);
		$task_list_todo = $model_project_task -> where($where_task_todo) -> order('end_date') -> get_list();
		$this -> assign('task_list_todo', $task_list_todo);

		//进行中
		$where_task_working[] = array('project_id', 'eq', $id);
		$where_task_working[] = array('is_finish', 'eq', 1);
		$where_task_working[] = array('is_del', 'eq', 0);
		$task_list_working = $model_project_task -> where($where_task_working) -> order('end_date') -> get_list();
		$this -> assign('task_list_working', $task_list_working);

		$where_task_finish[] = array('project_id', 'eq', $id);
		$where_task_finish[] = array('is_finish', 'eq', 3);
		$where_task_finish[] = array('is_del', 'eq', 0);
		$task_list_finish = $model_project_task -> where($where_task_finish) -> order('end_date') -> get_list();
		$this -> assign('task_list_finish', $task_list_finish);

		$where_task_close[] = array('project_id', 'eq', $id);
		$where_task_close[] = array('is_finish', 'eq', 4);
		$where_task_close[] = array('is_del', 'eq', 0);
		$task_list_close = $model_project_task -> where($where_task_close) -> order('end_date') -> get_list();
		$this -> assign('task_list_close', $task_list_close);

		$where_task_delay[] = array('project_id', 'eq', $id);
		$where_task_delay[] = array('is_del', 'eq', 0);
		$where_task_delay[] = array('is_finish', 'eq', 2);
		$task_list_delay = $model_project_task -> where($where_task_delay) -> order('end_date') -> get_list();

		$this -> assign('task_list_delay', $task_list_delay);

		$vo = model('project') -> find($id);
		$this -> assign('vo', $vo);

		$field_list = model("udf_field") -> get_data_list(0, $vo['udf_data']);
		$this -> assign("field_list", $field_list);

		$this -> display();
	}

	//修改
	public function edit($id) {

		$project = model('project');
		$vo = $project -> find($id);
		$this -> assign('vo', $vo);

		$field_list = model("udf_field") -> get_data_list(0, $vo['udf_data']);
		$this -> assign("field_list", $field_list);

		$project_user = model('project_member');
		//以选人员
		$where[] = array('project_id', 'eq', $id);
		$member = $project_user -> where($where) -> get_list();
		$this -> assign('member', $member);

		$this -> display();
	}

	protected function _insert($name = 'Project') {
		$model = model($name);
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> udf_data = model('udf_field') -> get_field_data();
		$list = $model -> add();
		if (false !== $list) {
			$member = request('member');
			model('project') -> set_member($member, $list);
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('添加成功!');
			//成功提示
		} else {
			$this -> error('编辑失败!');
			//错误提示
		}
	}

	protected function _update($name = 'Project') {
		$model = model($name);
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> udf_data = model('udf_field') -> get_field_data();
		$list = $model -> save();
		if (false !== $list) {
			$member = request('member');
			$project_id = request('id');
			model('project') -> set_member($member, $project_id);
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
			//成功提示
		} else {
			$this -> error('编辑失败!');
			//错误提示
		}
	}

	function del($id) {
		$this -> _del($id);
	}

	public function task_list() {
		$project_task = model('project_task');
		$project_task -> get_data();
		$where = $this -> _search($project_task);
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($where);
		}

		$where_project[] = array('user_id', 'eq', get_user_id());
		$project_list = model("project_member") -> where($where_project) -> get_field('project_id', true);
		if (!empty($project_list)) {
			$where[] = array('project_id', 'in', $project_list);
			$where[] = array('project_task.is_del', 'eq', 0);
		} else {
			$where[] = array('1', 'eq', '2');
		}
		$this -> _list($project_task, $where, 'id desc');
		$this -> display();
	}

	//新增任务
	public function add_task($project_id) {
		$this -> assign('project_id', $project_id);

		$model_flow_field = model("udf_field");
		$field_list = $model_flow_field -> get_field_list(1);
		$this -> assign("field_list", $field_list);

		$member_list = model('Project') -> get_member($project_id);
		$this -> assign('member_list', $member_list);

		$project = model('project') -> find($project_id);
		$this -> assign('project', $project);

		$this -> display();
	}

	//项目详细
	public function read_task($id) {

		$task = model('project_task') -> find($id);
		if ($task['executor_id'] == get_user_id()) {
			$this -> assign('is_edit', true);
		}

		$vo = model('project_task') -> find($id);
		$this -> assign('vo', $vo);

		$field_list = model("udf_field") -> get_data_list(1, $vo['udf_data']);
		$this -> assign("field_list", $field_list);

		$this -> display();
	}

	//修改任务
	public function edit_task($id) {

		$vo = model('project_task') -> find($id);
		$this -> assign('vo', $vo);

		$field_list = model("udf_field") -> get_data_list(1, $vo['udf_data']);
		$this -> assign("field_list", $field_list);

		$member = model('Project') -> get_member($vo['project_id']);
		$this -> assign('member_list', $member);

		$this -> display();
	}

	public function save_task() {
		$opmode = request('opmode');
		switch($opmode) {
			case "add" :
				$this -> _insert_task();
				break;
			case "edit" :
				$this -> _update_task();
				break;
			default :
				$this -> error("非法操作");
		}
	}

	protected function _insert_task() {
		$model = model('project_task');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> executor_name = get_user_name($model -> executor_id);
		$model -> udf_data = model('udf_field') -> get_field_data();
		$list = $model -> add();
		if (false !== $list) {
			$project_id = request('project_id');
			model('Project') -> calc_finish_rate($project_id);
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('添加成功!');
			//成功提示
		} else {
			$this -> error('添加失败!');
			//错误提示
		}
	}

	protected function _update_task() {
		$model = model('project_task');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		if ($model -> is_finish == 3) {
			$model -> finish_date = date('Y-m-d');
		}
		if (empty($model -> is_finish)) {
			$model -> udf_data = model('udf_field') -> get_field_data();
		}

		$model -> executor_name = get_user_name($model -> executor_id);
		$list = $model -> save();
		if (false !== $list) {
			$project_id = request('project_id');
			model('project') -> calc_finish_rate($project_id);
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
			//成功提示
		} else {
			$this -> error('编辑失败!');
			//错误提示
		}
	}

	//删除任务
	public function del_task($id) {
		$task = model('project_task') -> find($id);
		$auth = $this -> auth;
		if ($auth -> admin || $task['executor_id'] == get_user_id()) {
			$result = $this -> _del($id, 'project_task', true);
			if ($result !== false) {
				model('Project') -> calc_finish_rate($task['project_id']);
				$this -> success("成功删除{$result}条!");
			} else {
				$this -> error('删除失败');
			}
		} else {
			$this -> error('没有权限');
		}
	}

	//关闭任务
	public function close_task($id) {
		$task = model('project_task') -> find($id);
		$auth = $this -> auth;
		if ($auth -> admin || $task['user_id'] == get_user_id()) {
			$where[] = array('id', 'eq', $id);
			model('project_task') -> where($where) -> set_field('is_finish', 4);
			model('Project') -> calc_finish_rate($task['project_id']);
			$this -> success('任务关闭成功');
		} else {
			$this -> error('没有权限');
		}
	}

	function task_board() {
		$where_member[] = array('user_id', 'eq', get_user_id());
		$project_id_list = model('project_member') -> where($where_member) -> get_field('project_id', true);

		if (!empty($project_id_list)) {
			$where_project[] = array('is_del', 'eq', 0);
			$where_project[] = array('id', 'in', $project_id_list);
			$project_id_list = model('project') -> where($where_project) -> get_field('id', true);
		}

		if (!empty($project_id_list)) {
			$where_todo[] = array('project_id', 'in', $project_id_list);
			$where_process[] = array('project_id', 'in', $project_id_list);
			$where_finish[] = array('project_id', 'in', $project_id_list);
			$where_close[] = array('project_id', 'in', $project_id_list);

			$where_todo[] = array('executor_id', 'eq', get_user_id());
			$where_todo[] = array('is_finish', 'eq', 0);
			$list_todo = model('project_task') -> where($where_todo) -> get_list();
			$this -> assign('list_todo', $list_todo);

			$where_process[] = array('executor_id', 'eq', get_user_id());
			$where_process[] = array('is_finish', 'eq', 1);
			$list_process = model('project_task') -> where($where_process) -> get_list();
			$this -> assign('list_process', $list_process);

			$where_finish[] = array('executor_id', 'eq', get_user_id());
			$where_finish[] = array('is_finish', 'eq', 3);
			$list_finish = model('project_task') -> where($where_finish) -> get_list();

			$this -> assign('list_finish', $list_finish);

			$where_close[] = array('executor_id', 'eq', get_user_id());
			$where_close[] = array('is_finish', 'eq', 4);
			$list_close = model('project_task') -> where($where_close) -> limit(0, '10') -> get_list();
			$this -> assign('list_close', $list_close);
		}
		$this -> display();
	}

	function project_board($start_time = "", $end_time = "") {

		if ($start_time == "" && $end_time == "") {
			$start_time = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - date("w") - 30, date("Y")));
			$end_time = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - date("w") + 30, date("Y")));
		}

		$second1 = strtotime($start_time);
		$second2 = strtotime($end_time);

		if ($second1 < $second2) {
			$tmp = $second2;
			$second2 = $second1;
			$second1 = $tmp;
		}

		$day_diff = ($second1 - $second2) / 86400;

		$start_date = strtotime($start_time);
		$start_year = date('Y', $start_date);
		$start_month = date('m', $start_date);
		$start_day = date('d', $start_date);

		for ($i = 0; $i <= $day_diff; $i++) {
			$date[] = date("Y-m-d", mktime(0, 0, 0, $start_month, $start_day + $i, $start_year));
		}
		$this -> assign('date', $date);

		for ($i = 0; $i <= $day_diff; $i++) {
			$month[] = date("Y-m", mktime(0, 0, 0, $start_month, $start_day + $i, $start_year));
		}
		$this -> assign('month', array_count_values($month));

		for ($i = 0; $i <= $day_diff; $i++) {
			$week[] = date("W", mktime(0, 0, 0, $start_month, $start_day + $i, $start_year));
		}
		$this -> assign('week', array_count_values($week));

		$data['start_time'] = $start_time;
		$data['end_time'] = $end_time;

		$type = request("type");
		if (empty($type)) {
			$type = 'all';
		}

		$this -> assign('type', $type);

		if ($type == 'creater') {
			$where_project[] = array('user_id', 'eq', get_user_id());
			$project_id = model('project') -> where($where_project) -> get_field('id', true);
		}
		if ($type == 'actor') {
			$where_project[] = array('user_id', 'eq', get_user_id());
			$project_id = model('project_member') -> where($where_project) -> get_field('project_id', true);
		}
		if ($type == 'all') {
			$where_project[] = array('user_id', 'eq', get_user_id());
			$project_id1 = model('project_member') -> where($where_project) -> get_field('project_id', true);
			$project_id2 = model('project') -> where($where_project) -> get_field('id', true);
			if (empty($project_id1)) {
				$project_id1 = array();
			}
			if (empty($project_id2)) {
				$project_id2 = array();
			}

			$project_id = array_unique(array_merge($project_id1, $project_id2));
		}

		if (!empty($project_id)) {
			$where[] = array('id', 'in', $project_id);
		} else {
			$where['_string'] = '1=2';
		}

		$where[] = array('is_del', 'eq', 0);
		$where[] = array('start_date', 'elt', $end_time);
		$where[] = array('end_date', 'egt', $start_time);

		$list = model('project') -> where($where) -> get_list();

		$this -> assign('list', $list);

		$this -> assign('data', $data);
		$this -> display();
	}

	function project_field_manage() {
		$this -> assign("folder_name", "项目自定义字段");
		$this -> _field_manage(0);
	}

	function task_field_manage() {
		$this -> assign("folder_name", "任务自定义字段");
		$this -> _field_manage(1);
	}

	//上传
	function upload() {
		$this -> _upload();
	}

	//下载
	public function down($attach_id) {
		$this -> _down($attach_id);
	}

	public function download($file) {
		$this -> assign('file', $file);
		$this -> display();
	}

	public function select_member() {
		$dept_model = new model('dept');

		$list = array();
		$where[] = array('is_del', 'eq', 0);
		$list = $dept_model -> where($where) -> select('id,pid,name') -> order('sort asc') -> get_list();
		$tree = list_to_tree($list);
		$this -> assign('dept_tree', $tree);

		$position_model = new model('position');
		$list = $position_model -> select('id,name') -> order('sort asc') -> get_list();
		$tree = list_to_tree($list);
		$this -> assign('position_tree', $tree);

		$group_model = new model('group');
		$where = array();
		$where[] = array('user_id', 'eq', get_user_id());
		$list = $group_model -> where($where) -> select('id,name') -> order('sort asc') -> get_list();
		$list = list_to_tree($list);
		$this -> assign('list_group', popup_tree_menu($list));

		$this -> assign('type', 'dept');

		$this -> display();
	}

}
