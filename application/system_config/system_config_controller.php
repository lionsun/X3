<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class system_config_controller extends base_controller {
	protected $config = array('app_type' => 'public');

	public function index() {

		//显示左侧微信菜单树
		$system_license = get_system_config('system_license');
		$this -> assign('system_license', $system_license);

		$where_system[] = array('data_type', 'eq', 'system');
		$model = new model('system_config');
		$system_data = $model -> where($where_system) -> get_field('code,val');
		$this -> assign('system_data', $system_data);

		$this -> display();
	}

	function weixin() {
		$system_license = get_system_config('system_license');
		$this -> assign('system_license', $system_license);

		$where_weixin[] = array('data_type', 'eq', 'weixin');
		$model = new model('system_config');
		$weixin_data = $model -> where($where_weixin) -> get_field('code,val');
		$this -> assign('weixin_data', $weixin_data);

		$this -> display();
	}

	function weixin_menu() {
		$system_license = get_system_config('system_license');
		$this -> assign('system_license', $system_license);

		$model = model('weixin_menu');
		$list = $model -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('weixin_menu_list', $list);

		if (!empty($_POST['eq_pid'])) {
			$eq_pid = $_POST['eq_pid'];
		} else {
			$where_pid[] = array('pid', 'eq', '0');
			$eq_pid = $model -> where($where_pid) -> order('sort') -> get_field('id');
		}
		
		$where_pid_name[] = array('id', 'eq', $eq_pid);
		$pid_name = $model -> where($where_pid_name) -> order('sort asc') -> get_field('name');
		$this -> assign('eq_pid', $eq_pid);
		$this -> assign('pid_name', $pid_name);

		$where_node[] = array('pid', 'eq', 0);
		$top_node = $model -> where($where_node) -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('top_node', $top_node);
		
		$where_weixin_menu[] = array('pid', 'eq', $eq_pid);
		$weixin_menu = $model -> where($where_weixin_menu) -> select('id,pid,name,key,sort,type') -> order('sort') -> get_list();

		$tree = list_to_tree($weixin_menu, $eq_pid);
		$this -> assign('tree_menu', $tree);

		$this -> display();
	}

	//保存微信菜单
	public function add_weixin_menu() {
		$this -> display();
	}

	public function save_weixin_menu() {
		$this -> _save('weixin_menu');
	}

	public function del_weixin_menu($id) {
		$this -> _destory($id,'weixin_menu');
	}
	//获取微信菜单
	public function release() {
		import("weixin\Weixin");
		$weixin = new \Weixin();
		$agent_list = $weixin -> get_agent_list();

		if ($agent_list) {
			foreach ($agent_list as $key => $val) {
				$new[$val['agentid']] = $val['name'];
			}
			unset($new[0]);
		}

		$this -> assign('agent_list', $new);
		$this -> display();
	}

	function config() {
		$system_license = get_system_config('system_license');
		$this -> assign('system_license', $system_license);

		$menu = array();
		$where_common[] = array('data_type', 'eq', 'common');

		$model = new model('system_config');
		$menu = $model -> where($where_common) -> select('id,pid,name,is_del') -> order('sort') -> get_list();
		$tree = list_to_tree($menu);
		$this -> assign('tree_menu', $tree);

		$where_system_config[] = array('data_type', 'eq', 'common');
		$list = $model -> where($where_system_config) -> order('sort') -> get_field('id,name');
		$this -> assign('system_config_list', $list);

		$this -> display();
	}

	function push() {
		$system_license = get_system_config('system_license');
		$this -> assign('system_license', $system_license);

		$where_push[] = array('data_type', 'eq', 'push');

		$model = new model('system_config');
		$push_data = $model -> where($where_push) -> get_field('code,val');
		$this -> assign('push_data', $push_data);

		$this -> display();
	}

	function save() {
		//data_type 划分：
		$data_type = request('data_type');
		if ($data_type == 'system') {
			if (!isset($_REQUEST['login_verify_code'])) {
				$_REQUEST['login_verify_code'] = 0;
			}
			$this -> set_val('system_name', 'system');
			$this -> set_val('system_license', 'system');
			$this -> set_val('upload_file_ext', 'system');
			$this -> set_val('system_name', 'system');
			$this -> set_val('login_verify_code', 'system');
			$this -> success('保存成功');
			die ;
		}
		if ($data_type == 'weixin') {
			$this -> set_val('weixin_corp_id', 'weixin');
			$this -> set_val('weixin_secret', 'weixin');
			$this -> set_val('weixin_token', 'weixin');
			$this -> set_val('weixin_encoding_aes_key', 'weixin');
			$this -> set_val('weixin_site_url', 'weixin');
			$this -> success('保存成功');
			die ;
		}

		if ($data_type == 'system_push') {
			$this -> set_val('ws_push_config', 'push');
			$this -> set_val('ws_push_time', 'push');
			$this -> set_val('weixin_push_config', 'push');
			$this -> set_val('msg_push_config', 'push');
			$this -> success('保存成功');
			die ;
		}
		$this -> _save();
	}

	//保存微信应用
	public function add_weixin_option() {
		$model = M('weixin_option');
		$data['name'] = $_POST['name'];
		$data['sort'] = $_POST['sort'];
		$model -> add($data);
		$this -> success('新增成功！');
	}

	//发布
	public function weixin_update() {
		$node = M("weixin_menu");
		$menu = array();
		$where['option_id'] = $_SESSION['oid'];
		$menu = $node -> field('id,pid,name,url,key,type') -> where($where) -> order('sort ASC') -> select();

		foreach ($menu as $key => &$val) {
			if ($val['type'] == 'view') {
				$val['url'] = $this -> _get_weixin_auth_url($val['url']);
			}
		}
		//生成微信菜单所需格式。
		$menu_tree = $this -> create_tree($menu);
		$data['button'] = $menu_tree;
		$data = json_encode($data, JSON_UNESCAPED_UNICODE);
		$agent_id = $_POST['eq_pid'];
		import("Weixin.ORG.Util.Weixin");
		$weixin = new \Weixin();
		$rs = $weixin -> set_menu($data, $agent_id);
		if ($rs) {
			$this -> success('发布成功');
		} else {
			$this -> error($rs); ;
		}
	}

	// 创建Tree
	public function create_tree($list, $root = 0, $pk = 'id', $pid = 'pid', $child = 'sub_button') {
		$tree = array();
		if (is_array($list)) {
			// 创建基于主键的数组引用

			$refer = array();
			foreach ($list as $key => $data) {
				$refer[$data[$pk]] = &$list[$key];
			}

			foreach ($list as $key => $data) {
				// 判断是否存在parent
				$parentId = 0;
				if (isset($data[$pid])) {
					$parentId = $data[$pid];
				}
				if ((string)$root == $parentId) {
					$tree[] = &$list[$key];
				} else {
					if (isset($refer[$parentId])) {
						$parent = &$refer[$parentId];
						$parent[$child][] = &$list[$key];
					}
				}
			}
		}
		return $tree;
	}

	function set_val($key, $type) {
		$data['val'] = request($key);
		$data['data_type'] = $type;
		$where_system[] = array('code', 'eq', $key);

		$model = new model('system_config');
		$vo = $model -> where($where_system) -> find();
		if (!empty($vo)) {
			$data['id'] = $vo['id'];
			$list = $model -> save($data);
		} else {
			$data['code'] = $key;
			$list = $model -> add($data);
		}

		if ($list !== false) {
			return true;
		} else {
			return false;
		}
	}

	public function del($id) {
		$this -> _destory($id);
	}

	public function del_menu($id) {
		$model = M("WeixinMenu");
		$where['id'] = $id;
		$result = $model -> where($where) -> delete();
		if ($result) {
			$this -> success('删除成功！');
		}
	}

	//读取
	function edit($id) {
		$model = new model('weixin_menu');
		$vo = $model -> find($id);
		if (IS_AJAX) {
			if ($vo !== false) {// 读取成功
				$return['data'] = $vo;
				$return['status'] = 1;
				$return['info'] = "读取成功";
				ajax_return($return);
			} else {
				$return['status'] = 0;
				$return['info'] = "读取错误";
				ajax_return($return);
			}
		}
		$this -> assign('vo', $vo);
		$this -> display();
		return $vo;
	}

	public function select_pid() {
		$model = new model('system_config');
		$menu = array();
		$where[] = array('data_type', 'eq', 'common');
		$where[] = array('is_del', 'eq', 0);
		$menu = $model -> where($where) -> select('id,pid,name') -> order('sort asc') -> get_list();

		$tree = list_to_tree($menu);
		$this -> assign('tree', $tree);

		$this -> display();
	}

	public function winpop2() {
		$this -> winpop();
	}

	public function select_weixin_menu() {
		$weixin_menu = array();
		$weixin_menu = model("weixin_menu") -> select('id,pid,name') -> order('sort') -> get_list();

		$tree = list_to_tree($weixin_menu);
		$this -> assign('tree', $tree);

		$this -> display();
	}

	public function winpop4() {
		$this -> winpop3();
	}

	private function _get_weixin_auth_url($url) {
		$site_url = get_system_config("weixin_site_url");
		$corpid = get_system_config("weixin_corp_id");
		$redirect_uri = urlencode($site_url . '/index.php?m=Weixin');
		$url = base64_encode($site_url . $url);
		$oauth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$corpid}&redirect_uri={$redirect_uri}&response_type=code&scope=snsapi_base&state={$url}#wechat_redirect";
		return $oauth_url;
	}

	function timer() {

		$plugin['date'] = true;
		$this -> assign('plugin', $plugin);

		$model = new model('timer');
		$list = $model -> get_list();
		$this -> assign('list', $list);
		$tree = list_to_tree($list);

		$this -> assign('menu', sub_tree_menu($tree));
		$this -> display();
	}

	function add_timer() {
		$plugin['date'] = true;
		$this -> assign('plugin', $plugin);
		$this -> display();
	}

	function read_timer($id) {
		$model = model("timer");
		$data = $model -> find($id);
		if ($data !== false) {// 读取成功
			$return['data'] = $data;
			ajax_return($return);
		}
	}

	function save_timer() {
		$model = model("Timer");
		if (IS_POST) {
			$opmode = request('opmode');
			if (false === $model -> create()) {
				$this -> error($model -> getError());
			}
			if ($opmode == "add") {
				$list = $model -> add();
				if ($list != false) {
					$this -> success("添加成功");
				} else {
					$this -> error("添加成功");
				}
			}
			if ($opmode == "edit") {
				$list = $model -> save();
				if ($list != false) {
					$this -> success("保存成功");
				} else {
					$this -> error("保存失败");
				}
			}
			if ($opmode == "del") {
				$this -> _destory($model -> id, 'Timer');
			}
		}
	}

}
?>