<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class node_controller extends base_controller {
	private $auth_type = 'master';
	protected $auth_map = array('app_type' => 'master', 'admin' => 'node');

	public function index() {
		$node = new node_model();

		$list = $node -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('node_list', $list);

		if (!empty($_POST['eq_pid'])) {
			$eq_pid = $_POST['eq_pid'];
		} else {
			$where_pid[] = array('pid', 'eq', '0');
			$eq_pid = $node -> where($where_pid) -> order('sort asc') -> get_field('id');
		}

		$where_pid_name[] = array('id', 'eq', $eq_pid);
		$pid_name = $node -> where($where_pid_name) -> order('sort asc') -> get_field('name');
		$this -> assign('eq_pid', $eq_pid);
		$this -> assign('pid_name', $pid_name);

		$where_node[] = array('pid', 'eq', 0);
		$top_node = $node -> where($where_node) -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('top_node', $top_node);

		$menu = $node -> select('id,pid,name') -> order('sort asc') -> get_list();
		$tree = list_to_tree($menu, $eq_pid);
		$this -> assign('tree_menu', $tree);

		$this -> display();
	}

	protected function _insert($name = 'Node') {
		$model = new model('node');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		if (strpos($model -> url, '##') !== false) {
			$model -> sub_folder = ucfirst(get_controller(str_replace("##", "", $model -> url))) . "Folder";
		} else {
			$model -> sub_folder = '';
		}
		//保存当前数据对象
		$list = $model -> add();
		if ($list !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('新增成功!');
		} else {
			//失败提示
			$this -> error('新增失败!');
		}
	}

	protected function _update($name = 'Node') {
		$id = post('id');
		$model = new model('node');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		if (strpos($model -> url, '##') !== false) {
			$model -> sub_folder = ucfirst(get_controller(str_replace("##", "", $model -> url)));            
		} else {
			$model -> sub_folder = '';
		}
		// 更新数据
		$list = $model -> save();
		if (false !== $list) {
			//成功提示
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
		} else {
			//错误提示
			$this -> error('编辑失败!');
		}
	}

	function select_pid() {
		$dept_model = new model('node');
		$where[] = array('is_del', 'eq', 0);
		$menu = $dept_model -> where($where) -> select('id,pid,name') -> order('sort asc') -> get_list();

		$tree = list_to_tree($menu);
		$this -> assign('tree', $tree);
		$root['id'] = 0;
		$root['name'] = '根节点';
		$root['child'] = $tree;
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	function del($id) {
		$where_child[] = array('pid', 'eq', $id);
		$model = new model('node');
		$list = $model -> where($where_child) -> get_list();
		if ($list) {
			$this -> error('有子节点不能删除');
		}
		$model = new model('role_node');
		$where_role_node[]=array('node_id','eq',$id);
		$model -> where($where_role_node) -> delete();
		$this -> _destory($id);
	}

}
?>