<?php
/*---------------------------------------------------------------------------
  小微OA系统 - 让工作更轻松快乐 

  Copyright (c) 2013 http://www.smeoa.com All rights reserved.                                             


  Author:  jinzhu.yin<smeoa@qq.com>                         

  Support: https://git.oschina.net/smeoa/xiaowei               
 -------------------------------------------------------------------------*/


// 用户模型
namespace Home\Model;
use Think\Model;

class  MeetingModel extends CommonModel {
	public $_validate	=	array(
		array('start_time', 'check_time', '此时间段已有会议', 0, 'callback'),

		);

	function check_time(){
		$start_time = $_POST['start_time'].':00';
		$end_time = $_POST['end_time'].':00';
		$id = empty($_POST['id'])? 0:$_POST['id'];
		$rs=$this->db->query("select * from ".$this->tablePrefix."meeting 
			where DATE_FORMAT(`start_time`,'%Y-%m-%d')=DATE_FORMAT('$start_time','%Y-%m-%d') 
			and meeting_room=".$_POST['meeting_room']." and id <>".$id);

		$i=0;
		foreach ($rs as $key => $value) {
			if($rs[$key]['start_time']<=$start_time&&$start_time<=$rs[$key]['end_time']||$rs[$key]['start_time']<=$end_time&&$end_time<=$rs[$key]['end_time']||$rs[$key]['start_time']>=$start_time&&$rs[$key]['end_time']<=$end_time){
				$i++;
			}			
		}
		return $i > 0? false:true;
		
		

	}
	function _after_insert($data, $options) {
		$id = $data['id'];
		$actor_user_id = $data['actor_user_id'];
		$this -> _send_msg($id, $actor_user_id);
	}

	function _after_update($data, $options) {
		$id = $data['id'];
		$actor_user_id = $data['actor_user_id'];
		$this -> _del_actor($id);
		$this -> _send_msg($id, $actor_user_id);
	}
	private function _del_actor($id) {
		$where['meeting_id'] = array('eq', $id);

		$model = M("MeetingScope");
		$result = $model -> where($where) -> delete();

		if ($result === false) {
			return false;
		} else {
			return true;
		}
	}


	private function _send_msg($id, $user_list) {
		$user_list = array_filter(explode(",", $user_list));
		$return_user_list = array();
		foreach ($user_list as $val) {
			if (strpos($val, "dept_") !== false) {
				$dept_id = str_replace("dept_", '', $val);
				$dept_user = $this -> _get_user_list_by_dept_id($dept_id);
				if ($dept_user) {
					$return_user_list = array_merge($return_user_list, $dept_user);
				}
			} else {
				$return_user_list[] = $val;
			}
		}
		if (!empty($return_user_list)) {

			$info = M('Meeting') -> find($id);
			$info['meeting_room']=M('MeetingRoom') -> where(array('id'=>$info['meeting_room'])) -> get_field('name');
			$push_data['type'] = '会议';
			$push_data['action'] = $info['meeting_room'];
			$push_data['title'] = $info['name'].$info['start_time'].'-'.$info['end_time'];
			$push_data['content'] = del_html_tag($info['content']);
			$push_data['url'] = U('Meeting/read',"id={$id}&return_url=Info/index");
			
			send_push($push_data, $return_user_list);
			$return_user_list = implode(",", $return_user_list);
			$where = 'a.id in (' . $return_user_list . ') and b.id=\'' . $id . '\'';
			$sql = 'insert into ' . $this -> tablePrefix . 'meeting_scope (user_id,meeting_id) ';
			$sql .= ' select a.id, b.id from ' . $this -> tablePrefix . 'user a, ' . $this -> tablePrefix . 'meeting b where ' . $where;
			$result = $this -> execute($sql);

			if ($result === false) {
				return false;
			} else {
				return true;
			}
						
			

			

		} else {
			return true;
		}
	}

	private function _get_user_list_by_dept_id($id) {

		$dept = tree_to_list(list_to_tree( M("Dept") -> where('is_del=0') -> select(), $id));
		$dept = rotate($dept);

		if (empty($dept)) {
			$dept = array($id);
		} else {
			$dept = explode(",", implode(",", $dept['id']) . ",$id");
		}

		$model = M("User");
		$where['dept_id'] = array('in', $dept);
		$where['is_del'] = array('eq', 0);

		$data = $model -> where($where) -> get_field('id', true);
		return $data;
	}






	
}
	
?>