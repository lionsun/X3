<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/sef
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class dept_grade_controller extends base_controller {

    public function index() {
        $list = model('dept_grade') -> order('sort') -> get_list();
        $this -> assign('list', $list);
        $this -> display();
    }

    function del($id) {
        $this -> _destory($id);
    }

}
?>