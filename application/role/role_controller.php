<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

// 角色模块
use sef\controller;
use sef\model;

class role_controller extends base_controller {
    protected $config = array('app_type' => 'master');

    public function index() {
        $list = model('role') -> order('sort asc') -> get_list();
        $this -> assign('list', $list);
        $this -> display();
    }

    public function del() {
        $role_id = post('id');

        $model = new model('role_node');
        $model -> from('role_node');
        $where[] = array('role_id', 'eq', $role_id);
        $model -> where($where) -> delete();

        $model -> from('role_user');
        $model -> where($where) -> delete();

        $this -> _destory($role_id);
    }

    public function node() {
        if (!empty($_POST['eq_pid'])) {
            $eq_pid = $_POST['eq_pid'];
        } else {
            $eq_pid = "#";
        }
        $this -> assign('eq_pid', $eq_pid);

        $node_list = model('node') -> order('sort asc') -> get_list();

        if ($eq_pid != "#") {
            $node_list = tree_to_list(list_to_tree($node_list, $eq_pid));
        } else {
            $node_list = tree_to_list(list_to_tree($node_list));
        }

        $node_list = rotate($node_list, 'id');
        if ($eq_pid != "#") {
            $node_list[] = $eq_pid;
        }

        $where[] = array('id', 'in', $node_list);
        $node_list = model('node') -> select('id,pid,name,url') -> where($where) -> order('sort asc') -> get_list();

        $tree = list_to_tree($node_list);

        $list = tree_to_list($tree);
        $this -> assign('node_list', $list);

        $role_model = new model('role');
        $role_list = $role_model -> order('sort asc') -> get_list();
        $this -> assign('list', $role_list);

        $where_group[] = array('pid', 'eq', 0);
        $group_list = model('node') -> where($where_group) -> order('sort asc') -> get_field('id,name');
        $this -> assign('group_list', $group_list);

        $this -> display();
    }

    public function get_node_list() {
        $role_id = request('role_id');
        $model = new role_model();
        $data = $model -> get_node_list($role_id);
        if ($data !== false) {// 读取成功
            $return['data'] = $data;
            $return['status'] = 1;
            exit(json_encode($return));
        }
    }

    public function set_node() {
        //删除之前的节点;
        $role_id = post('role_id');
        $org_list = post('org_node_list');
        model('role') -> del_node($role_id, $org_list);

        $node_list = post('node_list');
        $admin_list = post('admin');
        $write_list = post('write');
        $read_list = post('read');

        $result = model('role') -> set_node($role_id, $node_list);

        if (!empty($admin_list)) {
            $where = array();
            $where[] = array('role_id', 'eq', $role_id);
            $where[] = array('node_id', 'in', $admin_list);
            $data['admin'] = 1;

            model('role_node') -> where($where) -> save($data);
        }
        if (!empty($write_list)) {
            $where = array();
            $where[] = array('role_id', 'eq', $role_id);
            $where[] = array('node_id', 'in', $write_list);
            $data['write'] = 1;
            model('role_node') -> where($where) -> save($data);
        }

        if (!empty($read_list)) {
            $where = array();
            $where[] = array('role_id', 'eq', $role_id);
            $where[] = array('node_id', 'in', $read_list);
            $data['read'] = 1;
            model('role_node') -> where($where) -> save($data);
        }

        if ($result === false) {
            $this -> error('操作失败！');
        } else {
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('操作成功！');
        }
    }

    public function user() {

        $map = $this -> _search();
        if (method_exists($this, '_search_filter')) {
            $this -> _search_filter($map);
        }
        $model = new model('user');
        $model -> select('user.*,dept.name dept_name,position.name position_name');
        $model -> inner('dept', 'user.dept_id=dept.id');
        $model -> inner('position', 'user.position_id=position.id');
        $user_list = $model -> where($map) -> get_list();
        //echo $model -> get_last_sql();
        //print_r($user_list);
        $this -> assign("user_list", $user_list);

        $role = new model('role');
        $role_list = $role -> order('sort asc') -> get_list();
        $this -> assign("role_list", $role_list);
        $this -> display();
    }

    public function get_role_list() {
        $id = request('id');
        $data = model('role') -> get_role_list($id);
        if ($data !== false) {// 读取成功
            $return['data'] = $data;
            $return['status'] = 1;
            exit(json_encode($return));
        }
    }

    public function set_role() {
        $user_list = post('user_id');
        $role_list = post('role_list');

        $model = model('role');
        $model -> del_role($user_list);

        $result = $model -> set_role($user_list, $role_list);
        if ($result === false) {
            $this -> error('操作失败！');
        } else {
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('操作成功！');
        }
    }
}
?>