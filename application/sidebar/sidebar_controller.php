<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use framework\controller;
use framework\model;

class sidebar_controller extends base_controller {
	protected $app_type = 'public';
	protected $is_widget = true;

	function left($tree_menu) {
		echo $this -> tree_nav($tree_menu);
	}

	function render($data) {
		$tree = $data['tree'];
		$badge_count = $data['new_count'];
		return $this -> tree_nav($tree);
	}

	function tree_nav($tree,$level = 0) {
		$level++;
		$html = "";
		if (is_array($tree)) {
			if ($level > 1) {
				$html = "<ul class='submenu collapse'>\r\n";
			} else {
				$html = "<ul id='side-menu' class='nav nav-list'>\r\n";
			}
			foreach ($tree as $val) {
				if (isset($val["name"])) {
					$title = $val["name"];
					if (!empty($val["url"])) {
						if (strpos($val['url'], "##") !== false) {
							$url = "#";
						} else if (strpos($val['url'], 'http') !== false) {
							$url = $val['url'];
						} else {
							$url = url($val['url']);
						}
					} else {
						$url = "#";
					}
					if (empty($val["id"])) {
						$id = $val["name"];
					} else {
						$id = $val["id"];
					}

					$icon = "fa fa-angle-right";

					if (isset($val['_child'])) {
						$html .= "<li>\r\n";
						$html .= "<a node=\"$id\" href=\"" . "$url\">";
						$html .= "<i class=\"$icon\"></i>";
						$html .= "<span class=\"menu-text\">$title</span>";
						$html .= "<span class=\"fa arrow\"></span>";
						$html .= "<span class=\"pull-right label label-primary badge-count node-{$val['id']}\"></span>";
						
						$html .= "</a>\r\n";
						$html .= $this -> tree_nav($val['_child'],$level);
						$html = $html . "</li>\r\n";
					} else {
						$html .= "<li>\r\n";
						$html .= "<a  node=\"$id\" href=\"" . "$url\">\r\n";
						$html .= "<i class=\"$icon\"></i>";
						$html .= "<span class=\"menu-text\">$title</span>";
						$html .= "<span class=\"pull-right label label-primary badge-count node-{$val['id']}\"></span>";
						$html .= "</a>\r\n</li>\r\n";
					}
				}
			}
			$html = $html . "</ul>\r\n";
		}
		return $html;
	}

}
?>