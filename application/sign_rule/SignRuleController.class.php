<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class SignRuleController extends HomeController {
	protected $config = array('app_type' => 'public');

	//分配规则
	function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$map['User.name|emp_no|Position.name|Dept.name'] = array('like', "%" . $keyword . "%");
		}
	}

	//考勤设置
	public function add() {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> display();
	}

	public function updata_save() {
		$sign = M('sign_rule');
		$id = $_POST['id'];
		$where['id'] = $id;

		$sign -> create();
		$rs = $sign -> where($where) -> save();
		if ($rs) {
			$this -> success('修改成功', U('sign_start'));
		}
	}

	//规则保存
	public function sign_set_save() {
		$sign_set = M('sign_rule');
		$data = $sign_set -> create();
		$data['create_name'] = get_user_name();
		$data['create_id'] = get_user_id();
		$data['create_time'] = date("Y-m-d h:i:s", time());
		$data['status'] = 1;
		//启用
		$rs = $sign_set -> add($data);
		if ($rs) {
			$this -> success('添加成功,请关闭', get_return_url());
		}
	}

	//规则删除
	public function del($contact_id) {
		$this -> _destory($contact_id, "sign_rule");
	}

 	public function get_role_list() {
		$model = M("sign_member");
		$id = request('id');
		$where['user_id'] = $id;
		$sid = $model -> where($where) -> getField('sid', true);
		if ($sid !== false) {// 读取成功
			$data['sid'] = $sid;
			$this -> ajaxReturn($data);
		}
	}

	public function member() {

		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$user_list = model("UserView") -> where($map) -> select();
		$this -> assign("user_list", $user_list);

		$role = M("SignRule");
		$role_list = $role -> select();
		$this -> assign("role_list", $role_list);
		
		$this -> display();
	}
	
	public function set_role() {
		$emp_list = $_POST["emp_id"];
		//user_id
		$role_list = $_POST["role_list"];
		//sid
		$sign = model("SignRule");
		$sign -> del_role($emp_list);
		$result = $sign -> set_role($emp_list, $role_list);

		if ($result === false) {
			$this -> error('操作失败！');
		} else {
			//$this -> assign('jumpUrl', get_return_url());
			$this -> success('保存成功', U('member'));
		}
	}

	//考勤统计
	public function sign_total() {
		$sign = M('sign');
		//签到记录
		$sign_gz = M('sign_rule');
		//签到
		$out_rule = M('sign_out_rule');
		//外出
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		if ($_GET['time'] == '') {
			$time = date('Y-m-d');
		} else {
			$time = $_GET['time'];
		}
		if ($_GET['type'] == '') {
		} else {
			$where1['status'] = 2;
		}

		$where1['sign_date'] = array("like", "%" . $time . "%");

		$_SESSION['time'] = $time;
		$day = date('Y-m-d', strtotime($time . "+1 day"));

		$sign_rule = M('SignRule') -> order('sort') -> getField('name', true);
		$where_sign['sign_date'] = array( array('egt', $time), array('elt', $day));
		$list = M('Sign') -> field("SUBSTRING(sign_date,1,10) sign_date,sign_name,status,user_id,emp_no,type") -> where($where_sign) -> group('user_id') -> select();
		foreach ($list as $val) {
			foreach ($sign_rule as $rule) {
				if ($val['sign_name'] == $rule) {
					$data[$val['emp_no']][$rule] = $val['status'];
				} else {
					$data[$val['emp_no']][$rule] = '3';
				}
			}
		}//签到表的处理方法

		$sign_rule1 = M('SignOutRule') -> getField('name', true);
		$where_sign1['sign_date'] = array( array('egt', $time), array('elt', $day));
		$list3 = M('Sign') -> field("SUBSTRING(sign_date,1,10) sign_date,sign_name,status,user_id,emp_no,type") -> where($where_sign) -> group('user_id') -> select();
		foreach ($list3 as $val) {
			foreach ($sign_rule1 as $rule) {
				if ($val['sign_name'] == $rule) {
					$data1[$val['emp_no']][$rule] = $val['status'];
				} else {
					$data1[$val['emp_no']][$rule] = '3';
				}
			}
		}//外出表的处理方法
		$head1 = $sign_gz -> order('id') -> select();
		$head2 = $out_rule -> order('id') -> select();
		//规则查询
		$this -> assign('head1', $head1);
		//sign_rule(签到表)提交页面
		$this -> assign('head2', $head2);
		//sign_out_rule（外出表）提交页面
		$this -> assign('data1', $data);
		//签到表
		$this -> assign('data2', $data1);
		//外出表
		//$this -> assign('type', $list);//
		$this -> assign('time', $time);
		//时间
		$this -> display();
	}

	public function export() {
		Header("Content-type:application/octet-stream");
		Header("Accept-Ranges:bytes");
		Header("Content-type:application/vnd.ms-excel");
		Header("Content-Disposition:attachment;filename=签到统计.xls");
		$Sign = M("sign");
		$time = $_SESSION['time'];
		$where['sign_date'] = array("like", "%" . $time . "%");
		$rs = $Sign -> where($where) -> select();
		Vendor('Excel.PHPExcel');
		//导入thinkphp第三方类库
		$objPHPExcel = new \PHPExcel();
		$i = 1;
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", '姓名') -> setCellValue("B$i", '签到时间') -> setCellValue("C$i", '签到地点') -> setCellValue("D$i", '签到规则') -> setCellValue("E$i", '签到状态') -> setCellValue("F$i", '其它') -> setCellValue("G$i", '备注');
		foreach ($rs as $val) {
			if ($val['status'] == 1) {
				$status = '正常';
			} else {
				$status = '异常';
			}
			if ($val['type'] == 'outside') {
				$type = "外勤";
			}
			$i++;
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", $val["emp_no"]) -> setCellValue("B$i", $val["sign_date"]) -> setCellValue("C$i", $val["location"]) -> setCellValue("D$i", $val["sign_name"]) -> setCellValue("E$i", $status) -> setCellValue("F$i", $type) -> setCellValue("G$i", $val["content"]);
		}
		//$objPHPExcel -> getActiveSheet() -> setTitle('Customer');
		$objPHPExcel -> setActiveSheetIndex(0);
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter -> save('php://output');
		exit ;
	}
}
?>