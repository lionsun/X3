<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;
use sef\auth;

class  system_folder_model extends base_model {

	function get_folder_list($controller = APP_NAME, $field = 'id,name,pid,sort') {
		$where[] = array('controller', 'eq', $controller);
		$where[] = array('is_del', 'eq', 0);
		$list = $this -> where($where) -> order("sort") -> select($field) -> get_list();
		return $list;
	}

	function get_folder_name($id) {
		$where[] = array('id', 'eq', $id);
		return $this -> where($where) -> get_field('name');
	}

	function get_folder_menu() {
		$where[] = array('is_del', 'eq', 0);
		$list = $this -> where($where) -> order('controller,sort desc') -> get_list();

		$data = array();
		$node_model = new model('node');
		foreach ($list as $key => $val) {
			$val['url'] = $val['controller'] . '/folder&fid=' . $val['id'];
			$val['badge_function'] = 'system_folder';
			$val['fid'] = $val['id'];
			$val['id'] = 'sfid_' . $val['id'];
			$val['pid'] = 'sfid_' . $val['pid'];

			if ($val["pid"] == 'sfid_0') {
				$where = array();
				$where[] = array('sub_folder', 'eq', $val['controller']);
				$pid = $node_model -> where($where) -> get_field('id');
				$val["pid"] = $pid;
			}

			$data[$key] = $val;
		}
		return $data;
	}

	function get_authed_folder($controller = APP_NAME) {
		$folder_list = array();
		$where[] = array('controller', 'eq', $controller);
		$list = $this -> where($where) -> get_field('id', true);		
		if ($list) {
			foreach ($list as $key => $val) {
				$auth = $this -> get_folder_auth($val);
				if ($auth -> read) {
					$folder_list[] = $val;
				}
			}
		}
		return $folder_list;
	}

	function get_folder_auth($id) {
		$auth = new auth();
		$where[] = array('id', 'eq', $id);
		$auth_list = $this -> where($where) -> select('system_folder.admin,system_folder.write,system_folder.read') -> find();
		if ($auth_list) {
			$result = array_map(array($this, "_check_auth"), $auth_list);
			$auth = new auth();
			if ($result['admin'] == true) {
				$auth -> admin = true;
				$auth -> write = true;
				$auth -> read = true;
			}
			if ($result['write'] == true) {
				$auth -> write = true;
				$auth -> read = true;
			}
			if ($result['admin'] == true) {
				$auth -> read = true;
			}
		}
		return $auth;
	}

	private function _check_auth($auth_list) {
		$arr = json_decode($auth_list);
		foreach ($arr as $item) {
			if (strpos($item -> data, "dept_") !== false) {
				$dept_id = substr($item -> data, 5);
				$emp_list = $this -> get_emp_list_by_dept_id($dept_id);
				if (empty($emp_list)) {
					return false;
				}
				$emp_list = rotate($emp_list, 'emp_no');
				if (in_array(get_emp_no(), $emp_list)) {
					return true;
				}
			} else {
				if (stripos($item -> data, get_emp_no()) !== false) {
					return true;
				}
			}
		}
		return false;
	}

	private function get_emp_list_by_dept_id($id) {
		$where[] = array('is_del', 'eq', '0');
		$dept_model = new model('dept');
		$list = $dept_model -> where($where) -> get_list();
		if (!empty($list)) {
			$dept = tree_to_list(list_to_tree($list, $id));
			$dept = rotate($dept);
			if (!empty($dept)) {
				$dept = $dept['id'];
				$dept[] = $id;
				$where[] = array('dept_id', 'in', $dept);
			} else {
				$where[] = array('dept_id', 'eq', $id);
			}
		}
		$user_model = new model('user');
		$data = $user_model -> where($where) -> get_list();
		return $data;
	}

}
?>