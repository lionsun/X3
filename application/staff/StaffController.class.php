<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class StaffController extends HomeController {
    //过滤查询字段
    protected $config = array('app_type' => 'common', 'read' => 'mobile','popup'=>'mobile');
    private $position;
    private $rank;
    private $dept;

    function _search_filter(&$map) {
        $map['name'] = array('like', "%" . $_POST['name'] . "%");
        $map['letter'] = array('like', "%" . $_POST['letter'] . "%");
        $map['is_del'] = array('eq', '0');
        if (!empty($_POST['tag'])) {
            $map['group'] = $_POST['tag'];
        }
        $map['user_id'] = array('eq', get_user_id());
    }

    function mobile() {
        $this -> assign("title", '职员查询');
        $node = model("Dept");
        $menu = array();
        $menu = $node -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
        $tree = list_to_tree($menu);
        $list = tree_to_list($tree);
        //dump($list);
        $this -> assign('menu', popup_tree_menu($tree));

        $keyword = request('keyword');

        if (!empty($keyword)) {
            $where['name'] = array('like', "%$keyword%");
            $where['emp_no'] = array('like', "%$keyword%");
            $where['letter'] = array('like', "%$keyword%");
            $where['dept_name'] = array('like', "%$keyword%");
            $where['mobile_tel'] = array('like', "%$keyword%");
            $where['_logic'] = "OR";

            $model = model("UserView");
            $field = 'id,emp_no,name,sex,birthday,dept_id,dept_name,position_id,office_tel,mobile_tel,email,duty,is_del,pic';
            $search = $model -> field($field) -> where($where) -> order('emp_no asc') -> select();
            //dump($search);
            $this -> assign('search', $search);
        }
        $this -> display();
    }

    function index2() {
        $node = model("Dept");
        $menu = array();
        $menu = $node -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
        $tree = list_to_tree($menu);
        $list = tree_to_list($tree);
        $this -> assign('menu', popup_tree_menu2($tree));
        $this -> display();
    }

    function index() {
        $this -> assign("title", '职员查询');
        $node = model("Dept");
        $menu = array();
        $menu = $node -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
        $tree = list_to_tree($menu);
        $list = tree_to_list($tree);
        //dump($list);
        $this -> assign('menu', popup_tree_menu($tree));

        $keyword = request('keyword');

        if (!empty($keyword)) {
            $where['name'] = array('like', "%$keyword%");
            $where['emp_no'] = array('like', "%$keyword%");
            $where['letter'] = array('like', "%$keyword%");
            $where['dept_name'] = array('like', "%$keyword%");
            $where['mobile_tel'] = array('like', "%$keyword%");
            $where['_logic'] = "OR";

            $model = model("UserView");
            $field = 'id,emp_no,name,sex,birthday,dept_id,dept_name,position_id,office_tel,mobile_tel,email,duty,is_del,pic';
            $search = $model -> field($field) -> where($where) -> order('emp_no asc') -> select();
            //dump($search);
            $this -> assign('search', $search);
        }
        $this -> display();
    }

    function read($id) {
        if (!empty($id)) {
            $model = M("Dept");
            $dept = tree_to_list(list_to_tree( M("Dept") -> where('is_del=0') -> select(), $id));
            $dept = rotate($dept);
            $dept = implode(",", $dept['id']) . ",$id";

            $where['is_del'] = array('eq', '0');
            $where['dept_id'] = array('eq', $id);
        }

        //dump($where);
        $model = model("UserView");
        $field = 'id,emp_no,name,sex,birthday,dept_id,dept_name,position_id,position_name,office_tel,mobile_tel,email,duty,is_del,pic';
        $data = $model -> field($field) -> where($where) -> order('emp_no asc') -> select();
        $return['data'] = $data;
        $return['status'] = 1;
        $this -> ajaxReturn($return);
    }

}
?>