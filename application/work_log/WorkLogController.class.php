<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class WorkLogController extends HomeController {
	protected $config = array('app_type' => 'common', 'read' => 'index2,good,get_work_log,user_list,audit,set_audit,get_user_list,sub', 'write' => 'comment,reply,content_item,plan_item', 'admin' => 'all');
	//过滤查询字段
	function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		if (!empty($_POST['keyword'])) {
			$where['content'] = array('like', '%' . $_POST['keyword'] . '%');
			$where['plan'] = array('like', '%' . $_POST['keyword'] . '%');
			$where['user_name'] = array('like', '%' . $_POST['keyword'] . '%');
			$where['_logic'] = 'or';
			$map['_complex'] = $where;
		}
	}

	public function index($type = 0) {
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('user_id', get_user_id());

		$auth = $this -> auth;
		$this -> assign('auth', $auth);
		if ($auth->admin) {
			$node = model("Dept");
			$dept_id = get_dept_id();
			$dept_name = get_dept_name();

			$menu = array();
			$dept_menu = $node -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
			$dept_tree = list_to_tree($dept_menu, $dept_id);
			$count = count($dept_tree);
			if (empty($count)) {
				/*获取部门列表*/
				$html = '';
				$html = $html . "<option value='{$dept_id}'>{$dept_name}</option>";
				$this -> assign('dept_list', $html);
				/*获取人员列表*/
				$where['dept_id'] = array('eq', $dept_id);
				$emp_list = model("User") -> where($where) -> getField('id,name');
				$this -> assign('emp_list', $emp_list);
			} else {
				/*获取部门列表*/
				$this -> assign('dept_list', select_tree_menu($dept_tree));
				$dept_list = tree_to_list($dept_tree);
				$dept_list = rotate($dept_list);
				$dept_list = $dept_list['id'];

				/*获取人员列表*/
				$where['dept_id'] = array('in', $dept_list);
				$emp_list = model("User") -> where($where) -> getField('id,name');
				$this -> assign('emp_list', $emp_list);
			}
		}

		$model = model("WorkLogView");
		$map = $this -> _search($model);
		if ($auth->admin) {
			if (empty($map['dept_id'])) {
				if (!empty($dept_list)) {
					$map['dept_id'] = array('in', array_merge($dept_list, array($dept_id)));
				} else {
					$map['dept_id'] = array('eq', $dept_id);
				}
			}
		} else {
			$map['user_id'] = get_user_id();
		}

		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
		switch($type) {
			case 0 :
				$map['type'] = 0;
				$work_title = "日报";
				break;
			case 1 :
				$map['type'] = 1;
				$work_title = "周报";
				break;
			case 2 :
				$map['type'] = 2;
				$work_title = "季报";
				break;
			case 3 :
				$map['type'] = 3;
				$work_title = "年报";
				break;
		}
		$this -> assign('work_title', $work_title);
		$this -> assign('type', $type);
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	function sub() {
		$where = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($where);
		}
		$model = model("WorkLogView");

		$dept_id = get_dept_id();
		$sub_dept = get_sub_dept($dept_id);

		$sub_duty_user_list = model('Duty') -> get_sub_duty_user_list(get_user_id());
		$sub_position_user = model('Position') -> get_sub_position_user_list(get_user_id());
		if (empty($sub_duty_user_list) or empty($sub_position_user)) {
			$where['_string'] = '1=2';
		} else {
			$user_list = array_intersect($sub_duty_user_list, $sub_position_user);
			$where['user_id'] = array('in', $user_list);
			$where['dept_id'] = array('in', $sub_dept);
		}

		if (!empty($model)) {
			$this -> _list($model, $where);
		}
		$this -> display();
	}

	function edit($id) {
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);

		$this -> _edit($id);
	}

	public function add($type = 0) {
		$this -> assign('type', $type);
		$this -> display();
	}

	public function get_work_log($id) {
		$list = M("WorkLog") -> find($id);
		$html = W('FileUpload/html', array('add_file' => $list['add_file']));
		$list['html'] = $html;

		//$where_comment['row_id'] = array('eq', $id);
		//$where_comment['controller'] = array('eq', 'WorkLog');
		//$comment = M('Comment') -> where($where_comment) -> select();
		//$list['comment'] = $comment;
		$this -> ajaxReturn($list);
	}

	public function good($id) {
		$where['id'] = array('eq', $id);
		$good = M('WorkLog') -> where($where) -> getField('good');
		$good = json_decode($good, true);
		$user_id = get_user_id();
		if (in_array($user_id, array_keys($good))) {
			unset($good[$user_id]);
			$return['status'] = 0;
		} else {
			$good[$user_id] = get_user_name();
			$return['status'] = 1;
		}
		M('WorkLog') -> where($where) -> setField('good', json_encode($good, JSON_UNESCAPED_UNICODE));
		$return['data'] = $good;
		$this -> ajaxReturn($return);
	}

	public function comment($row_id) {
		$model = model("Comment");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> type = 'comment';
		$model -> user_id = get_user_id();
		$model -> user_name = get_user_name();
		$model -> create_time = time();
		$model -> controller = APP_NAME;
		$list = $model -> add();

		$where['row_id'] = array('eq', $row_id);
		$where['controller'] = array('eq', APP_NAME);
		$comment = M('Comment') -> where($where) -> select();

		if ($list !== false) {
			$return['stauts'] = 1;
			$return['comment'] = $comment;
			$return['info'] = '评论成功';
		} else {
			$return['stauts'] = 0;
			$return['info'] = '评论失败';
		}
		$this -> ajaxReturn($return);
	}

	public function reply($row_id) {
		$model = model("Comment");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> type = 'reply';
		$model -> user_id = get_user_id();
		$model -> user_name = get_user_name();
		$model -> create_time = time();
		$model -> controller = APP_NAME;
		$list = $model -> add();

		$where['row_id'] = array('eq', $row_id);
		$where['controller'] = array('eq', APP_NAME);
		$comment = M('Comment') -> where($where) -> select();

		if ($list !== false) {
			$return['stauts'] = 1;
			$return['comment'] = $comment;
			$return['info'] = '回复成功';
		} else {
			$return['stauts'] = 0;
			$return['info'] = '回复失败';
		}
		$this -> ajaxReturn($return);
	}

	function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}

	/** 插入新新数据  **/
	protected function _insert($name = "WorkLog") {
		$model = model($name);
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$db_fields = $model -> getDbFields();
		if (in_array('user_id', $db_fields)) {
			$model -> user_id = get_user_id();
		};
		if (in_array('user_name', $db_fields)) {
			$model -> user_name = get_user_name();
		};
		if (in_array('dept_id', $db_fields)) {
			$model -> dept_id = get_dept_id();
		};
		if (in_array('dept_name', $db_fields)) {
			$model -> dept_name = get_dept_name();
		};
		$model -> create_time = time();
		/*保存当前数据对象 */
		$model -> content_item_id = json_encode($model -> content_item_id);
		$model -> content_item = json_encode($model -> content_item);
		$model -> content = json_encode($model -> content);
		$model -> finish = json_encode($model -> finish);
		$model -> suggest = json_encode($model -> suggest);
		$model -> plan_item_id = json_encode($model -> plan_item_id);
		$model -> plan_item = json_encode($model -> plan_item);
		$model -> plan_content = json_encode($model -> plan_content);
		$model -> duty = json_encode($model -> duty);

		$list = $model -> add();
		if ($list !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('新增成功!');
		} else {
			$this -> error('新增失败!');
			//失败提示
		}

	}

	public function user_list() {
		$plugin['jquery-ui'] = true;
		$this -> assign("plugin", $plugin);

		$this -> _dept_list();
		$this -> _position_list();

		$this -> assign('type', 'dept');
		$this -> display();
		return;
	}

	private function _dept_list() {
		$model = M("Dept");
		$list = array();
		$list = $model -> where('is_del=0') -> field('id,pid,name') -> order('sort asc') -> select();
		$list = list_to_tree($list);
		$this -> assign('list_dept', popup_tree_menu($list));
	}

	private function _position_list() {
		$model = M("Position");
		$list = array();
		$list = $model -> field('id,name') -> order('sort asc') -> select();
		$list = list_to_tree($list);
		$this -> assign('list_position', popup_tree_menu($list));
	}

	public function audit() {
		$model = model("WorkLogView");
		$map = $this -> _search($model);
		$map['audit_id'] = get_user_id();
		$map['is_save'] = 1;
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	public function set_audit($id, $status) {
		if ($status == 0) {
			$status = "不通过";
		} elseif ($status == 1) {
			$status = "通过";
		} else {
			$this -> error('非法操作');
		}
		$worklog = M('WorkLog');
		$data['id'] = $id;
		$data['status'] = $status;
		$rs = $worklog -> save($data);

		if ($rs !== 0) {
			$this -> success('操作成功');
		} else {
			$this -> error('操作失败');
		}
	}

	public function del($id) {
		$this -> _del($id);
	}

	public function get_user_list($dept_id) {
		$userview = model('UserView');
		$where['dept_id'] = $dept_id;
		$list = $userview -> where($where) -> getField('id,name', true);
		$this -> ajaxReturn($list);
	}

	public function content_item() {
		$node = M("WorkLogItem");
		$menu = array();
		if (!empty($_POST['keyword'])) {
			$where['name'] = array('like', '%' . $_POST['keyword'] . '%');
		}
		$menu = $node -> where($where) -> field('id,name') -> select();
		$this -> assign('menu', popup_tree_menu($menu));
		$this -> display();
	}

	public function plan_item() {
		$node = M("WorkLogItem");
		$menu = array();
		if (!empty($_POST['keyword'])) {
			$where['name'] = array('like', '%' . $_POST['keyword'] . '%');
		}
		$menu = $node -> where($where) -> field('id,name') -> select();
		$this -> assign('menu', popup_tree_menu($menu));
		$this -> display();
	}

}
