<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  user_folder_model extends base_model {

	function get_folder_list($controller = APP_NAME, $field = 'id,name,pid,sort') {
		$where[] = array('controller', 'eq', $controller);
		$where[] = array('is_del', 'eq', 0);
		$where[] = array('user_id', 'eq', get_user_id());
		$list = $this -> where($where) -> order("sort") -> select($field) -> get_list();
		return $list;
	}

	function get_folder_name($id) {
		$where[] = array('id', 'eq', $id);
		$where[] = array('user_id', 'eq', get_user_id());
		return $this -> where($where) -> get_field('name');
	}

	function get_folder_menu() {
		$where[] = array('is_del', 'eq', 0);
		$list = $this -> where($where) -> order('controller,sort asc') -> get_list();

		$data = array();
		$node_model = new model('node');
		foreach ($list as $key => &$val) {
			$val['badge_function'] = 'badge_count_user_folder';
			$val['fid'] = $val['id'];
			$val['id'] = 'ufid_' . $val['id'];
			$val['pid'] = 'ufid_' . $val['pid'];

			if ($val["pid"] == 'ufid_0') {
				$where = array();
				$where[] = array('sub_folder', 'eq', $val['controller'] . "Folder");
				$pid = $node_model -> where($where) -> get_field('id');
				$val["pid"] = $pid;
			}
		}

		return $data;
	}

	public function _get_folder_auth($folder_id) {
		return array('admin' => true, "write" => true, "read" => true);
	}

}
?>