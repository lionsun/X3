<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class product_controller extends base_controller {
	protected $config = array('app_type' => 'personal');

	//过滤查询字段
	function _search_filter(&$map) {
		$map[] = array('user_id', 'eq', get_user_id());
		$map[] = array('is_del', 'eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$where[] = array('name', 'like', $keyword, 'or');
			$where[] = array('company', 'like', $keyword, 'or');
			$where[] = array('dept', 'like', $keyword, 'or');
			$where[] = array('position', 'like', $keyword, 'or');
			$where[] = array('office_tel', 'like', $keyword, 'or');
			$where[] = array('mobile_tel', 'like', $keyword, 'or');
			$where[] = array('letter', 'like', $keyword, 'or');
			$map[] = array('complex', $where);
		}
	}

	function index() {
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
		$model = model("product");
		if (!empty($model)) {
			if (request('mode') == 'export') {
				$this -> _index_export($model, $map);
			} else {
				$this -> _list($model, $map);
			}
		}

		$this -> display();
	}

	function _index_export() {
		$model = M("product");
		$where['user_id'] = array('eq', get_user_id());
		$where['is_del'] = array('eq', 0);
		$list = $model -> where($where) -> select();

		//导入thinkphp第三方类库
		Vendor('Excel.PHPExcel');

		//$inputFileName = "Public/templete/product.xlsx";
		// $objPHPExcel = \PHPExcel_IOFactory::load($inputFileName);
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel -> getProperties() -> setCreator("smeoa") -> setLastModifiedBy("smeoa") -> setTitle("Office 2007 XLSX Test Document") -> setSubject("Office 2007 XLSX Test Document") -> setDescription("Test document for Office 2007 XLSX, generated using PHP classes.") -> setKeywords("office 2007 openxml php") -> setCategory("Test result file");
		// Add some data
		$i = 1;
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", "姓名") -> setCellValue("B$i", "单位") -> setCellValue("C$i", "部门") -> setCellValue("D$i", "职位") -> setCellValue("E$i", "办公电话") -> setCellValue("F$i", "手机") -> setCellValue("G$i", "邮箱") -> setCellValue("H$i", "QQ") -> setCellValue("I$i", "网站") -> setCellValue("J$i", "地址") -> setCellValue("K$i", "其他");
		//dump($list);
		foreach ($list as $val) {
			$i++;
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", $val["name"]) -> setCellValue("B$i", $val["company"]) -> setCellValue("C$i", $val["dept"]) -> setCellValue("D$i", $val["position"]) -> setCellValue("E$i", $val["office_tel"]) -> setCellValue("F$i", $val["mobile_tel"]) -> setCellValue("G$i", $val["email"]) -> setCellValue("H$i", $val["im"]) -> setCellValue("I$i", $val["website"]) -> setCellValue("J$i", $val["address"]) -> setCellValue("J$i", $val["remark"]);
		}
		// Rename worksheet
		$objPHPExcel -> getActiveSheet() -> setTitle('product');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel -> setActiveSheetIndex(0);
		$file_name = "product.xlsx";
		// Redirect output to a client’s web browser (Excel2007)
		header("Content-Type: application/force-download");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition:attachment;filename =" . str_ireplace('+', '%20', URLEncode($file_name)));
		header('Cache-Control: max-age=0');

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		//readfile($filename);
		$objWriter -> save('php://output');
		exit ;
	}

	public function import() {
		$opmode = request('opmode');
		if ($opmode == "import") {
			$file = model('file');
			$info = $file -> upload($_FILES['uploadfile']);
			if (!$info) {
				$this -> error($File -> getError());
			} else {
				//取得成功上传的文件信息
				vendor();
				//导入thinkphp第三方类库
				$upload_path = config('upload_path');
				$upload_path = 'uploads';

				$file_path = $upload_path . DS . $info["save_path"] . DS . $info["save_name"];

				$objPHPExcel = \PHPExcel_IOFactory::load($file_path);
				$sheetData = $objPHPExcel -> getActiveSheet() -> toArray(null, true, true, true);
				$model = model("product");
				for ($i = 2; $i <= count($sheetData); $i++) {
					$data = array();
					$data['name'] = mb_substr($sheetData[$i]["A"], 0, 50);
					$data['company'] = mb_substr($sheetData[$i]["B"], 0, 50);
					$data['letter'] = mb_substr(get_letter($sheetData[$i]["A"]), 0, 50);
					$data['dept'] = mb_substr($sheetData[$i]["C"], 0, 50);
					$data['position'] = mb_substr($sheetData[$i]["D"], 0, 50);
					$data['email'] = mb_substr($sheetData[$i]["G"], 0, 50);
					$data['office_tel'] = mb_substr($sheetData[$i]["E"], 0, 50);
					$data['mobile_tel'] = mb_substr($sheetData[$i]["F"], 0, 50);
					$data['website'] = mb_substr($sheetData[$i]["I"], 0, 50);
					$data['im'] = mb_substr($sheetData[$i]["H"], 0, 50);
					$data['address'] = mb_substr($sheetData[$i]["J"], 0, 50);
					$data['user_id'] = get_user_id();
					$data['remark'] = mb_substr($sheetData[$i]["K"], 0, 50);
					$data['is_del'] = 0;
					$model -> add($data);
				}
				//dump($sheetData);
				if (file_exists(__ROOT__ . "/" . $file_path)) {
					unlink(__ROOT__ . "/" . $file_path);
				}
				$this -> assign('jumpUrl', get_return_url());
				$this -> success('导入成功！');
			}
		} else {
			$this -> display();
		}
	}

	function tag_manage() {
		$this -> _system_tag_manage("分组管理",true);		
	}

	function del($id) {
		$this -> _del($id);
	}

	function set_tag() {
		$id = $_POST['id'];
		$tag = $_POST['tag'];
		$new_tag = $_POST['new_tag'];
		if (!empty($id)) {
			$model = model("UserTag");
			$model -> del_data_by_row($id);
			if (!empty($_POST['tag'])) {
				$result = $model -> set_tag($id, $tag);
			}
		};

		if (!empty($new_tag)) {
			$model = model("UserTag");
			$model -> controller = APP_NAME;
			$model -> name = $new_tag;
			$model -> is_del = 0;
			$model -> user_id = get_user_id();
			$new_tag_id = $model -> add();
			if (!empty($id)) {
				$result = $model -> set_tag($id, $new_tag_id);
			}
		};
		if ($result !== false) {//保存成功
			if ($ajax || IS_AJAX)
				$this -> assign('jumpUrl', get_return_url());
			$this -> success('操作成功!');
		} else {
			//失败提示
			$this -> error('操作失败!');
		}
	}

	protected function _insert($name = 'product') {
		$model = model('product');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> letter = get_letter($model -> name);
		//保存当前数据对象
		$list = $model -> add();
		if ($list !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('新增成功!');
		} else {
			//失败提示
			$this -> error('新增失败!');
		}
	}

	protected function _update($name = 'product') {
		$id = $_POST['id'];
		$model = model("product");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> letter = get_letter($model -> name);
		// 更新数据
		$list = $model -> save();
		if (false !== $list) {
			//成功提示
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
		} else {
			//错误提示
			$this -> error('编辑失败!');
		}
	}

	protected function _assign_tag_list() {
		$model = model("UserTag");
		$tag_list = $model -> get_tag_list('id,name');
		$this -> assign("tag_list", $tag_list);
	}

	function folder_manage() {
		$this -> _system_folder_manage('产品分类', true);
	}

	function field_manage() {
		$this -> assign("folder_name", "商品自定义字段");
		$this -> _field_manage(0);
	}
}
?>