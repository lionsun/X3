<?php
use sef\controller;
use sef\model;

class index_controller extends base_controller {
	protected $app_type = 'system';

	public function index() {
		cookie('top_menu_id',255);
		redirect(url('kf/index'));		 
	}
}
