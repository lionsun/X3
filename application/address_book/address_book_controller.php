<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class address_book_controller extends base_controller {
	private $position;
	private $rank;
	private $dept;

	function index() {
		$this -> assign("title", '职员查询');
		$menu = array();
		$where_dept[] = array('is_del', 'eq', 0);
		$menu = model('dept') -> select('id,pid,name,is_del') -> where($where_dept) -> order('sort asc') -> get_list();
		$tree_menu = list_to_tree($menu);
		$this -> assign('tree_menu', $tree_menu);

		$keyword = request('keyword');

		if (!empty($keyword)) {
			$where[] = array('user.name', 'like', $keyword, 'or');
			$where[] = array('emp_no', 'like', $keyword, 'or');
			$where[] = array('letter', 'like', $keyword, 'or');
			$where[] = array('dept.name', 'like', $keyword, 'or');
			$where[] = array('position.name', 'like', $keyword, 'or');
			$where[] = array('mobile_tel', 'like', $keyword, 'or');
			$map[] = array('complex', $where);
			$map[] = array('user.is_del', 'eq', 0);

			$model = model('user');
			$model -> left('dept', 'dept.id=user.dept_id');
			$model -> left('position', 'position.id=user.position_id');
			$model -> select('user.id,emp_no,user.name,sex,birthday,dept_id,dept.name dept_name,position_id,position.name position_name,office_tel,mobile_tel,email,duty,pic');

			$search = $model -> where($map) -> order('emp_no asc') -> get_list();

			//dump($search);
			$this -> assign('search', $search);
		}
		$this -> display();
	}

	function mobile() {
		$this -> assign("title", '职员查询');
		$menu = array();
		$where_dept[] = array('is_del', 'eq', 0);
		$menu = model('dept') -> select('id,pid,name,is_del') -> where($where_dept) -> order('sort asc') -> get_list();
		$tree_menu = list_to_tree($menu);
		$this -> assign('tree_menu', $tree_menu);

		$keyword = request('keyword');

		if (!empty($keyword)) {
			$where[] = array('user.name', 'like', $keyword, 'or');
			$where[] = array('emp_no', 'like', $keyword, 'or');
			$where[] = array('letter', 'like', $keyword, 'or');
			$where[] = array('dept.name', 'like', $keyword, 'or');
			$where[] = array('position.name', 'like', $keyword, 'or');
			$where[] = array('mobile_tel', 'like', $keyword, 'or');
			$map[] = array('complex', $where);
			$map[] = array('user.is_del', 'eq', 0);

			$model = model('user');
			$model -> left('dept', 'dept.id=user.dept_id');
			$model -> left('position', 'position.id=user.position_id');
			$model -> select('user.id,emp_no,user.name,sex,birthday,dept_id,dept.name dept_name,position_id,position.name position_name,office_tel,mobile_tel,email,duty,pic');

			$search = $model -> where($map) -> order('emp_no asc') -> get_list();

			//dump($search);
			$this -> assign('search', $search);
		}
		$this -> display();
	}

	function index2() {
		$node = D("Dept");
		$menu = array();
		$menu = $node -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
		$tree = list_to_tree($menu);
		$list = tree_to_list($tree);
		$this -> assign('menu', popup_tree_menu2($tree));
		$this -> display();
	}

	function read($id) {
		if (!empty($id)) {
			$sub_dept = get_sub_dept($id);
			$where[] = array('user.dept_id', 'in', $sub_dept);
		}

		//dump($where);
		$model = model('user');
		$model -> left('dept', 'dept.id=user.dept_id');
		$model -> left('position', 'position.id=user.position_id');
		$model -> select('user.id,emp_no,user.name,sex,birthday,dept_id,dept.name dept_name,position_id,position.name position_name,office_tel,mobile_tel,email,duty,pic');

		$where[] = array('user.is_del', 'eq', 0);

		$list = $model -> where($where) -> order('emp_no asc') -> get_list();
		$return['data'] = $list;
		$return['status'] = 1;
		ajax_return($return);
	}

}
?>