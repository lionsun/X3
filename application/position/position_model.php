<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

// 节点模型
use sef\model;

class  position_model extends base_model {

    public function get_sub_position_user($user_id) {
        $where1[] = array('id', 'eq', $user_id);
        $position_id = model('user') -> where($where1) -> get_field('position_id');

        $where2[] = array('id', 'eq', $position_id);
        $position_sort = model('position') -> where($where2) -> get_field('sort');

        $sub_position = ceil($position_sort / 10 + 0.1) * 10;

        $where3[] = array('sort', 'egt', $sub_position);
        $sub_position_id_list = $this -> where($where3) -> get_field('id', true);

        $where3[] = array('position_id', 'in', $sub_position_id_list);
        return model('user') -> where($where3) -> get_field('id', true);
    }

}
?>