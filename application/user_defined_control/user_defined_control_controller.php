<?php
use sef\controller;
use sef\model;

class user_defined_control_controller extends base_controller {
	protected $config = array('app_type' => 'public');

	public function edit($data) {
		
		if(!isset($data['val'])){
			$data['val'] = null;
		}
		if ($data['val'] == "") {
			$data['val'] = null;
		}
		if (strpos($data['val'], '|') !== false) {
			$data['val'] = explode('|', $data['val']);
		}
		if ($data['type'] == 'compute_field') {
			$data['val'] = $this -> compute($data['data']);
		}

		if (in_array($data['validate'], array('dy', 'xy', 'dydy', 'xydy'))) {
			$field_name = $data['config'];
			$where['row_type'] = array('eq', $data['row_type']);
			$where['name'] = array('eq', $field_name);
			$field_id = M('UdfField') -> where($where) -> getField('id');
			$data['validate'] .= '|udf_field_' . $field_id;
		}

		$data['data'] = $this -> _conv_data($data['data']);
		$this -> assign('data', $data);

		return $this -> fetch('user_defined_control/view/' . $data['type'] . '.html');
	}

	private function _conv_data($val) {
		$new = array();
		if (strpos($val, 'CONFIG:') !== false) {
			$code = substr($val, 7);
			$model = M('SystemConfig');
			$where['code'] = array('eq', $code);
			return $model -> where($where) -> field('id,pid,name') -> order('sort asc') -> select();
		}
		if (strpos($val, 'SYSTEM:') !== false) {
			$data_type = substr($val, 7);
			if ($data_type == 'DEPT') {
				return M('Dept') -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
			}
			if ($data_type = 'RPODUCT') {
				$dept_menu = M('Dept') -> field('id,pid,name') -> where("is_del=0") -> order('sort asc') -> select();
				$dept_tree = list_to_tree($dept_menu);
				$list = tree_to_list($dept_tree);
				foreach ($list as $val) {
					$new[$val['id']] = str_pad("", $val['level'] * 3, "│") . "├─" . "{$val['name']}";
				}
			}
			return $new;
		}

		if (strpos($val, "|") !== false) {
			$arr_tmp = explode(",", $val);
			foreach ($arr_tmp as $item) {
				$tmp = explode("|", $item);
				$new[$tmp[0]] = $tmp[1];
			}
			return $new;
		}
		return $val;
	}

	function compute($str) {

		//$str = '{$2}*{$2}*{#0.7}-{$3}/{#7}-{$3}';
		// $str = '{$33333333333}-{$33333333333}';

		$pattern = '/\{([\$|#][^\}]*)\}/';

		if (preg_match_all($pattern, $str, $vars)) {
			//print_r($vars);
		}
		$vars = $vars[1];
		foreach ($vars as $key => $val) {
			if (strpos($val, '$') !== false) {
				$vars[$key] = $this -> get_val(substr($val, 1));
			}
			if (strpos($val, '#') !== false) {
				$vars[$key] = substr($val, 1);
			}
		}
		$pattern = '/[\+,\-,\*,\/]/';
		//$keywords = preg_split($pattern, $content);
		if (preg_match_all($pattern, $str, $compute)) {
			//print_r($compute);
		}
		foreach ($compute[0] as $key => $val) {
			if ($val == '*') {
				//echo $vars[$key];
				$vars[$key] = $vars[$key] * $vars[$key + 1];
				$vars[$key + 1] = &$vars[$key];
				// echo '<br>';
				// echo '*' . $vars[$key + 1];
				// echo '<br>-------------<br>';
				// echo '=' . $vars[$key];
				//  echo '<br>';
				//  echo '<br>';
			};
			if ($val == '/') {
				//echo $vars[$key];
				$vars[$key] = $vars[$key] / $vars[$key + 1];
				//echo '<br>';
				//echo '/' . $vars[$key + 1];
				//echo '<br>-------------<br>';
				//echo '=' . $vars[$key];
				//echo '<br>';
				//echo '<br>';
			};
		}
		$ret = $vars[0];
		foreach ($compute[0] as $key => $val) {
			if ($val == '+') {
				//echo $ret;
				$ret = $ret + $vars[$key + 1];
				//echo '<br>';
				//echo '+' . $vars[$key + 1];
				//echo '<br>-------------<br>';
				//echo '=' . $ret;
				//echo '<br>';
				//echo '<br>';
			};
			if ($val == '-') {
				//echo $ret;
				$ret = $ret - $vars[$key + 1];
				//echo '<br>';
				//echo '-' . $vars[$key + 1];
				//echo '<br>-------------<br>';
				//echo '=' . $ret;
				//echo '<br>';
				//echo '<br>';
			};
		}
		return $ret;
	}

	function get_val($val) {
		$where['name'] = array('eq', $val);
		$vars = M('DataCompute') -> where($where) -> find();
		$year = date('Y');
		$month = date('m');
		$day = date('d');

		$where['field_id'] = array('eq', $vars['collection_id']);

		if ($vars['period'] == 'y') {
			$time = mktime(0, 0, 0, 1, 1, $year);
			$where['date_time'] = array('egt', $time);
		}
		if ($vars['period'] == 'h') {
			if ($month < 7) {
				$month = 1;
			} else {
				$momth = 7;
			}
			$time = mktime(0, 0, 0, $momth, 1, $year);
			$where['date_time'] = array('egt', $time);
		}
		if ($vars['period'] == 'q') {
			if ($month <= 3) {
				$month = 1;
			} elseif ($month <= 6) {
				$month = 4;
			} elseif ($month <= 9) {
				$month = 7;
			} elseif ($month < 12) {
				$month = 10;
			}
			$time = mktime(0, 0, 0, $momth, 1, $year);
			$where['date_time'] = array('egt', $time);
		}
		if ($vars['period'] == 'm') {
			$time = mktime(0, 0, 0, $month, 1, $year);
			$where['date_time'] = array('egt', $time);
		}
		if ($vars['scope'] = 'user') {
			$where['user_id'] = array('eq', get_user_id());
		}
		if ($vars['scope'] = 'dept') {
			$where['dept_id'] = array('eq', get_dept_id());
		}
		$list = M('UdfData') -> where($where) -> getField('data_val', true);

		if ($vars['mode'] == 'sum') {
			return array_sum($list);
		}
		if ($vars['mode'] == 'count') {
			return count($list);
		}
		if ($vars['mode'] == 'avg') {
			return array_sum($list) / count($list);
		}
		sort($list);
		if ($vars['mode'] == 'max') {
			return end($list);
		}
		if ($vars['mode'] == 'min') {
			return $list[0];
		}
	}

}
?>